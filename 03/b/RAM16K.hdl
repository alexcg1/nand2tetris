// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/03/b/RAM16K.hdl

/**
 * Memory of 16K registers, each 16 bit-wide. Out holds the value
 * stored at the memory location specified by address. If load==1, then 
 * the in value is loaded into the memory location specified by address 
 * (the loaded value will be emitted to out from the next time step onward).
 */

CHIP RAM16K {
    IN in[16], load, address[14];
    OUT out[16];

    PARTS:
    DMux4Way(in=load, a=t00, b=t01, c=t10, d=t11, sel=address[12..13]);

    RAM4K(in=in, load=t00, address=address[0..11], out=RAM4Kout0);
    RAM4K(in=in, load=t01, address=address[0..11], out=RAM4Kout1);
    RAM4K(in=in, load=t10, address=address[0..11], out=RAM4Kout2);
    RAM4K(in=in, load=t11, address=address[0..11], out=RAM4Kout3);
    
    Mux4Way16(a=RAM4Kout0, b=RAM4Kout1, c=RAM4Kout2, d=RAM4Kout3, sel=address[12..13], out=out);
}