

// Connect screen registers to tracker
// Create countdown so avoid going past screen

(COUNTER)
	@8193 // Screen has 8192 bits (excluding first bit which we do pre-loop)
	D=A
	@countdown // New variable to track countdown from last screen register. TODO: Set this to 16384 since this is total number of screen registers
	M=D

	// Go to (screen -1), since loop adds 1 to register each time
	@16382
	// Write (SREEEN-1) to @regtrack
	D=A
	@regtrack
	M=D
	D=M

(KEYLOOP)

	// Start keyboard check
	@KBD
	D=M
	
	// Loop back to keyboard check start if no input detected
	//@KEYLOOP
	//D; JEQ

	// If KBD == 0, reset screen
	@RESET
	D; JEQ

	// Go to BLACKSCREEN if keyboard input
	@BLACKSCREEN
	D; JNE

	@END
	0; JMP

(BLACKLOOP)

	// Access reg tracker
	@regtrack
	A=M+1 // Go to address+1
	M=-1 // Write -1 to address
	D=A // push current address to D

	// Add 1 to regtracker
	@regtrack
	M=M+1

	// Countdown -1
	@countdown
	M=M-1
	D=M

	// If countdown == 0, end loop and go back to keyloop
	@KEYLOOP
	D; JEQ

	// Otherwise continue the loop
	@BLACKLOOP
	0; JMP

(RESET)
	@1
	M=M+1

	@KEYLOOP
	0; JMP

(END)
	@END
	0; JMP