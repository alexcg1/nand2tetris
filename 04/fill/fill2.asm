// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

	// Set screenreg to first register in SCREEN
	@SCREEN
	D=A
	@screenreg
	M=D

	// Set counter to total of all registers in screen (8k)
	@8000
	D=A
	@counter
	M=D


(DARK)
	@0
	D=A
	@output
	M=D-1

	// screenreg = screenreg+16 (because each reg is 16 bits long)
	@16
	D=A
	@
	M=M+1

	// Set next register to contents of screenreg
	A=M
	M=-1

@DARK
0; JMP


	@END
	0;JMP

(END)
	@END
	0; JMP