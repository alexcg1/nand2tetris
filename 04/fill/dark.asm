// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Set screenreg to first register in SCREEN - keeps count of each screen register
	@SCREEN
	D=A-1
	@screenreg // register 16
	M=D

	// Set counter to total of all registers in screen (8k)
	// Set to 4k for now
	//@8192 // 8k
	@8191 // Leaves last register free for keyboard
	//@4000
	D=A+1
	@counter // register 17
	M=D

(KEYLOOP)

// Get keyboard input
	@KBD
	M=D

// If keyboard input != 0, goto DARK
	@DARK
	D; JNE

// Loop back to KEYLOOP
	@KEYLOOP
	0; JMP


(DARK)
// Starting loop to make screen dark

// Check counter. If counter == 0, we can end the program
	@counter
	D=M
	@END
	D; JEQ

	// screenreg = screenreg+16 (because each reg is 16 bits long)
	@16
	D=A
	@screenreg



	M=M+1 // Bug: This keeps going past 24576 (the last memory register) Perhaps put a goto end if M is too big

	// Set next register contents to -1 (i.e. "black")
	A=M // Bug cont'ed
	M=-1

	// Start: Minus one from counter
	@counter
	M=M-1
	D=M
	M=D
	// End: Minus one from counter

	// Start: If counter > zero, loop back round
	@counter
	D=M
	@DARK
	D; JGT
	// End: If counter != zero, loop back round

	// Start: If counter == 0, do something else
	@counter
	D=M
	@END
	D; JEQ
	// End: If counter == 0, do something else


	// Putting 'goto end' just in case loop gets fucked
	@END
	0; JMP

(FOO)
	// Just writing some dummy test data
	@R3
	M=1

	// Then end program
	@END
	0; JMP