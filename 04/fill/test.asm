// Keyboard stuff
(KEY)
	// while no button pressed, show light, else dark
	// if KEYB == 0, GOTO LIGHT
	// else, GOTO DARK

	// Put keyboard input into D
	@KBD
	D=M

	@key
	M=D

	// If D == 0, no key is pressed, so go to LIGHT
	//@LIGHT
	//D; JEQ

	// If D != 0, goto DARK
	//@DARK
	//D; JNE

	@KEY
	0; JMP

	// Stuff

	(LIGHT) // Not tested yet
	@0
	D=A
	@output to zero //register 18
	M=D

	// screenreg = screenreg+16 (because each reg is 16 bits long)
	@16
	D=A
	@screenreg
	M=M+1

	// Set next register contents to zero
	A=M
	M=0

	// Minus one from counter
	@counter
	M=M-1
	D=M

	@KEY
	0; JMP

	@DARK
	0; JMP