// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Set screenreg to first register in SCREEN - keeps count of each screen register
	@SCREEN
	D=A-1
	@screenreg // register 16
	M=D

	// Set counter to total of all registers in screen (8k)
	// Set to 4k for now
	//@8192
	@4000
	D=A+1
	@counter // register 17
	M=D

// Keyboard stuff
(KEY)
	// while no button pressed, show light, else dark
	// if KEYB == 0, GOTO LIGHT
	// else, GOTO DARK

	// Put keyboard input into D
	@KBD
	D=M

	// If D == 0, no key is pressed, so go to LIGHT
	@LIGHT
	D; JEQ

	// If D != 0, goto DARK
	@DARK
	D; JNE

	@KEY
	0; JMP

(DARK)
	@0
	D=A
	@output //register 18
	M=D-1

	// screenreg = screenreg+16 (because each reg is 16 bits long)
	@16
	D=A
	@screenreg
	M=M+1

	// Set next register contents to -1
	A=M
	M=-1

	// Minus one from counter
	@counter
	M=M-1
	D=M

	@DARK
	0; JMP

	@KEY
	0; JMP




(LIGHT) // Not tested yet
	@0
	D=A
	@output to zero //register 18
	M=D

	// screenreg = screenreg+16 (because each reg is 16 bits long)
	@16
	D=A
	@screenreg
	M=M+1

	// Set next register contents to zero
	A=M
	M=0

	// Minus one from counter
	@counter
	M=M-1
	D=M

	@KEY
	0; JMP

	@DARK
	0; JMP


(END)
	@END
	0; JMP