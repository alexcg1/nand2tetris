// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Set reg0 to R0
	@R0
	D=M

	@reg0 // Register 16
	M=D

// Set reg1 to R1
	@R1
	D=M

	@reg1 // Reg 17
	M=D

// Set count to reg0, and minus 1 away from it until 0 to stop the loop
// Need to minus one to start with, since we're setting sum to reg0 as starting value
	@reg0
	D=M-1

	@count // Reg 18
	M=D

// Set sum to reg1
	@reg1
	D=M
	@sum // Reg 19
	M=D

(LOOP)

// Add reg1 to sum (sum starts as reg1, so keep adding reg1 until count == 0)
	@reg1
	D=M
	M=D

	@sum
	M=M+D

	@count //register 18
	M=M-1

// When count == 0, write sum to R2

	@count
	D=M
	M=D

	@WRITEOUT
	D; JEQ

// Otherwise, continue loop
	@LOOP
	0; JMP

(WRITEOUT)
	@sum
	D=M

	@R2
	M=D

	@END
	0; JMP

(END)
	@END
	0;JMP