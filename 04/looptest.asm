// Basic program to test loops and counters in assembly

// Set up variables

// Connect screen registers to tracker
@SCREEN
D=A

@screenregtrack
M=D
D=A
// End connect to tracker

// Write -1 to first reg in screen
@SCREEN
M=-1





// Set up countdown to only goto LOOP2 if countdown > 0
// Set countdown to 10
@10
D=A

@countdown
M=D

(LOOP1)
	@R1
	M=M+1

	// Only goto LOOP2 if countdown above 0
		@countdown
		D=M
		@LOOP2
		D; JGT
	// End countdown check

	// After countdown = 0, end program
	@END
	0; JMP

		(LOOP2)
			@R2
			M=M+1

			@countdown
			M=M-1

			@LOOP1
			0; JMP
		// End LOOP2

	// This stuff shouldn't run because it's out of the loop

	@R3
	M=M+1

(END)
@END
0; JMP